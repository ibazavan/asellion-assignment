CREATE DATABASE product;
CREATE USER test WITH PASSWORD 'test1234';
GRANT ALL PRIVILEGES ON DATABASE product TO test;
\connect product
CREATE TABLE public.product
(
    id serial NOT NULL,
    name text NOT NULL,
    current_price DECIMAL NOT NULL,
    last_update timestamp without time zone,
    CONSTRAINT users_pkey PRIMARY KEY (id)
);
ALTER TABLE public.product
    OWNER to test;