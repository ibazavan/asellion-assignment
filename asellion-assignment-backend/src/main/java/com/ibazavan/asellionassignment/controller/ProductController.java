package com.ibazavan.asellionassignment.controller;

import com.ibazavan.asellionassignment.model.dto.ProductDto;
import com.ibazavan.asellionassignment.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping
    public List<ProductDto> getAllProducts() {
        return productService.getProducts();
    }

    @GetMapping(value = "/1", params = "name")
    public ProductDto getProductByName(@RequestParam String name) {
        return productService.getProduct(name);
    }


    @GetMapping(value = "/1", params = "id")
    public ProductDto getProductById(@RequestParam Long id) {
        return productService.getProduct(id);
    }

    @PostMapping
    public ResponseEntity postProduct(@RequestBody @Valid ProductDto postDto) {
        return productService.saveProduct(postDto);
    }

    @PutMapping("/1")
    public ResponseEntity putProduct(@RequestBody @Valid ProductDto productDto) {
        return productService.updateProduct(productDto);
    }

}
