package com.ibazavan.asellionassignment.service;

import com.ibazavan.asellionassignment.model.Product;
import com.ibazavan.asellionassignment.model.dto.ProductDto;
import com.ibazavan.asellionassignment.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<ProductDto> getProducts() {
        log.info("Retrieving all products..");
        return toProductDtos(productRepository.findAll());
    }

    public ProductDto getProduct(String name) {
        log.info("Attempting to retrieve product with name: " + name);
        Product product = productRepository.findByName(name);
        if (product == null) {
            log.error("No product found with name: " + name);
            throw new IllegalArgumentException("No product found with given name.");
        }
        return toProductDto(product);
    }

    public ProductDto getProduct(long id) {
        log.info("Attempting to retrieve product with id: " + id);
        Product product = productRepository.findById(id).get();
        if (product == null) {
            log.error("No product found with id: " + id);
            throw new IllegalArgumentException("No product found with given id.");
        }
        return toProductDto(product);
    }


    public ResponseEntity saveProduct(ProductDto productDto) {
        log.info("Saving new product..");
        productRepository.save(toProduct(productDto));
        return new ResponseEntity(HttpStatus.OK);
    }

    public ResponseEntity updateProduct(ProductDto productDto) {
        Product product = productRepository.findById(productDto.getId()).get();
        if (product == null) {
            log.error("No product found with name: " + productDto.getName());
            throw new IllegalArgumentException("No product found with given name.");
        }
        updateAndSave(productDto, product);
        return new ResponseEntity(HttpStatus.OK);
    }

    private void updateAndSave(ProductDto productDto, Product product) {
        product.setCurrentPrice(productDto.getCurrentPrice());
        product.setName(productDto.getName());
        product.updateTimestamps();
        productRepository.save(product);
    }

    private List<ProductDto> toProductDtos(List<Product> products) {
        return products.stream().map(product -> toProductDto(product)).collect(Collectors.toList());
    }

    private ProductDto toProductDto(Product product) {
        return modelMapper.map(product, ProductDto.class);
    }

    private Product toProduct(ProductDto productDto) {
        return modelMapper.map(productDto, Product.class);
    }
}
