package com.ibazavan.asellionassignment.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ProductDto {

    public ProductDto() {
        this.lastUpdate = LocalDateTime.now();
    }

    private Long id;
    @NotNull
    private String name;
    @NotNull
    private BigDecimal currentPrice;
    private LocalDateTime lastUpdate;

}
