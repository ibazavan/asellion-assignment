package com.ibazavan.asellionassignment.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @Column(name = "current_price")
    private BigDecimal currentPrice;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate;

    public void updateTimestamps(){
        setLastUpdate(LocalDateTime.now());
    }
}
