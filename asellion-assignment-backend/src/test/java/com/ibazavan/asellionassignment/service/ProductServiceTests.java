package com.ibazavan.asellionassignment.service;

import com.ibazavan.asellionassignment.model.Product;
import com.ibazavan.asellionassignment.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ProductServiceTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void getProducts() {
        //given
        Product product1 = new Product();
        product1.setName("Carbon");
        entityManager.persist(product1);
        entityManager.flush();

        Product product2 = new Product();
        product2.setName("Titanium");
        entityManager.persist(product2);
        entityManager.flush();

        //when
        List<Product> products = productRepository.findAll();

        //then
        assertThat(products.size()).isEqualTo(2);
        assertThat(products.get(0)).isEqualTo(product1);
        assertThat(products.get(1)).isEqualTo(product2);
    }

    @Test
    public void getProduct() {
        //given
        Product product = new Product();
        product.setName("Carbon");
        entityManager.persist(product);
        entityManager.flush();

        //when
        Product testProduct = productRepository.findById(product.getId()).get();

        //then
        assertThat(testProduct.getName()).isEqualTo(product.getName());
    }
}